import {GLTFLoader} from "../bibliotheques/GLTFLoader.js";

export let scene, camera, renderer;
export let model, panier;
export let ballonPris, ballonLancer, clientConnect, posBallonInitX, posBallonInitY, posBallonInitZ;

let ground, colRimGauche, colRimDroite, colRimCentre, colPanneau;
let pointage = 0, peutCompter = true, peutCollisionObjet = true;
let physics_stats, render_stats, loader;

export class Espace {

    constructor() {

        // Instanciation du client Socket.IO pour communiquer avec le serveur
        this.socket = io("https://dstonge.dectim.ca:3002", {
            query: { type: 'jeu' } // identification en tant que jeu
        });

        this.socket.on("joueur1", message => {
            document.getElementById("messages").innerText += "joueur1: " + JSON.stringify(message) + "\n";
        });

        this.socket.on("joueur2", message => {
            document.getElementById("messages").innerText += "joueur2: " + JSON.stringify(message)  + "\n";
        });

        Physijs.scripts.worker = '../bibliotheques/physijs_worker.js';
        Physijs.scripts.ammo = 'ammo.js';

        this.initLoader();
        this.initThreeJS();

        this.ecouteur = {
            ecouteurTicker: this.detecterCollisionPoints.bind(this)
        };

        // Sons
        this.musiqueArcade = createjs.Sound.play('', {loop:-1});
        this.musiqueArcade.volume = 0.2;

        createjs.Ticker.addEventListener('tick', this.ecouteur.ecouteurTicker)

    }

    initLoader() {

        loader = new GLTFLoader();

        loader.load('ressources/panierEspace.glb', function (gltf) {

            panier = gltf.scene.children[0];
            scene.add(gltf.scene);
            console.log(scene);

        }, undefined, function (error) {

            console.error(error);

        });

    }

    initThreeJS() {
        scene = new Physijs.Scene({fixedTimeStep: 1 / 120});
        scene.setGravity(new THREE.Vector3(0, -30, 0));
        scene.addEventListener(
            'update',
            function () {
                scene.simulate(undefined, 2);
                physics_stats.update();
            }
        );

        camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 0.1, 1000);


        renderer = new THREE.WebGLRenderer({antialias: true});
        renderer.setSize(window.innerWidth, window.innerHeight);
        renderer.shadowMap.enabled = true;
        renderer.shadowMapSoft = true;
        document.body.appendChild(renderer.domElement);

        render_stats = new Stats();
        render_stats.domElement.style.position = 'absolute';
        render_stats.domElement.style.top = '0px';
        render_stats.domElement.style.zIndex = 100;
        document.body.appendChild(render_stats.domElement);

        physics_stats = new Stats();
        physics_stats.domElement.style.position = 'absolute';
        physics_stats.domElement.style.top = '50px';
        physics_stats.domElement.style.zIndex = 100;
        document.body.appendChild(physics_stats.domElement);


        this.ajouterVideo();
        this.ajouterElements();
        this.ajouterTableauPoints();

    }

    ajouterElements() {
        //Lumière ambiante

        let ambient = new THREE.AmbientLight(0x000000, 0.3);
        scene.add(ambient)
        let pointLight = new THREE.PointLight(0xffffff, 0.6, 200);
        pointLight.position.set(10, 10, 100);
        pointLight.castShadow = true;
        scene.add(pointLight);

        //Lumière directionnelle

        let dirLight = new THREE.DirectionalLight(0xffffff, 1, 100);
        dirLight.position.set(20, 15, 2);
        dirLight.castShadow = true;
        scene.add(dirLight);

        //Plancher        ----------------------------------------------//

        let material = new Physijs.createMaterial(new THREE.MeshBasicMaterial({
            color: 0x888888,
            opacity: 1,
            transparent: true
        }), 0, 1);
        ground = new Physijs.BoxMesh(
            new THREE.BoxGeometry(30, 0.3, 100),
            material,
            0 // mass
        );

        ground.position.y = -10;
        ground.position.z = -30;
        ground.receiveShadow = true;
        ground.rotation.x = 0.04;
        scene.add(ground);

        //BOUNDS COLLISION ----------------------------------------------//

        colPanneau = new Physijs.BoxMesh(
            new THREE.BoxGeometry(13, 9, 0.3),
            material,
            0 // mass
        );
        colPanneau.position.z = -26;
        colPanneau.position.y = 10;
        scene.add(colPanneau);

        let colPoteau = new Physijs.BoxMesh(
            new THREE.BoxGeometry(1, 25, 1),
            material,
            0 // mass
        );
        colPoteau.position.z = -27;
        colPoteau.position.y = 0;
        scene.add(colPoteau);


        colRimGauche = new Physijs.BoxMesh(
            new THREE.BoxGeometry(0.2, 0.2, 8),
            material,
            0 // mass
        );

        colRimGauche.position.z = -26;
        colRimGauche.position.y = 7;
        colRimGauche.position.x = -3;
        scene.add(colRimGauche);

        colRimDroite = new Physijs.BoxMesh(
            new THREE.BoxGeometry(0.3, 0.3, 8),
            material,
            0 // mass
        );

        colRimDroite.position.z = -26;
        colRimDroite.position.y = 7;
        colRimDroite.position.x = 3;
        scene.add(colRimDroite);

        colRimCentre = new Physijs.BoxMesh(
            new THREE.BoxGeometry(6, 0.3, 0.3),
            material,
            0 // mass
        );

        colRimCentre.position.z = -21;
        colRimCentre.position.y = 7;

        scene.add(colRimCentre);

        //Ballon
        let sphere_geometry = new THREE.SphereGeometry(2, 32, 32);

        let materialModel = new Physijs.createMaterial(new THREE.MeshBasicMaterial({map: new THREE.TextureLoader().load('./ressources/texture-ballon.png')}), 0, 1);
        model = new Physijs.SphereMesh(sphere_geometry, materialModel, 10);
        model.position.set(0, 0, 0);
        model.setCcdMotionThreshold(1);
        model.setCcdSweptSphereRadius(0.2);

        model.addEventListener('collision', this.detecterCollisionObjets.bind(this));

        posBallonInitX = model.position.x;
        posBallonInitY = model.position.y;
        posBallonInitZ = model.position.z;

        scene.add(model);

        model.addEventListener('lancer', this.lancerBallon.bind(this));

        camera.position.z = 10;

        this.animate();
    }

    ajouterVideo() {

        let video, videoImage, videoImageContext, videoTexture;

        init();
        animate();

        // FUNCTIONS
        function init() {
            video = document.createElement('video');
            video.type = 'video';
            video.src = "ressources/video/";
            video.load(); // must call after setting/changing source
            video.play();
            video.loop = true;

            videoImage = document.createElement('canvas');
            videoImage.width = 1280;
            videoImage.height = 720;

            videoImageContext = videoImage.getContext('2d');
            // background color if no video present
            videoImageContext.fillRect(0, 0, videoImage.width, videoImage.height);

            videoTexture = new THREE.Texture(videoImage);
            videoTexture.minFilter = THREE.LinearFilter;
            videoTexture.magFilter = THREE.LinearFilter;

            let movieMaterial = new THREE.MeshBasicMaterial({
                map: videoTexture,
                overdraw: true,
                side: THREE.DoubleSide
            });
            let movieGeometry = new THREE.PlaneGeometry(128, 72, 4, 4);
            let movieScreen = new THREE.Mesh(movieGeometry, movieMaterial);
            movieScreen.position.set(0, 0, -50);
            scene.add(movieScreen);

        }

        function animate() {
            requestAnimationFrame(animate);
            render();
            // update();
        }

        function render() {
            if (video.readyState === video.HAVE_ENOUGH_DATA) {
                videoImageContext.drawImage(video, 0, 0, 1280, 720);
                if (videoTexture)
                    videoTexture.needsUpdate = true;
            }

            renderer.render(scene, camera);
        }
    }

    ajouterTableauPoints(){

        console.log('affichage du tableau');
        // Image du tableau
        let loader = new THREE.TextureLoader();
        let material = new THREE.MeshLambertMaterial({
            map: loader.load('ressources/images/ScoreEspace.png')
        });

        material.transparent = true;

        var geometry = new THREE.PlaneGeometry(3.5, 5*1);
        var mesh = new THREE.Mesh(geometry, material);
        mesh.position.set(-7,0,0);
        scene.add(mesh);

        var light = new THREE.PointLight( 0xffffff, 1, 0 );
        light.position.set(1, 1, 100 );
        scene.add(light)

    }

    lancerBallon(temps, posXinit, posYinit, posXfinal, posYfinal) {

        if (!ballonLancer) {

            ballonPris = true;
            ballonLancer = true;

            let tempsParcours = temps / 100;
            let distanceY = (posYinit - posYfinal) / 2;

            let velocite = distanceY / 5;

            //Calculer l'angle du lancé
            let angle = ((posXinit - posXfinal) * -1) / 10;

            console.log("angle de: " + angle);

            console.log(velocite);

            model.setLinearVelocity(new THREE.Vector3(angle, velocite, -velocite));
            model.setAngularVelocity(new THREE.Vector3(angle, angle, angle));

            setTimeout(() => {

                model.__dirtyPosition = false;
                model.position.set(posBallonInitX, posBallonInitY, posBallonInitZ);
                ballonPris = false;
                ballonLancer = false;
                model.__dirtyPosition = true;
                peutCompter = true;

            }, 4000);
        }
    }

    detecterCollisionPoints() {

        if (model.position.x >= colRimGauche.position.x &&
            model.position.z <= colRimCentre.position.z &&
            model.position.x <= colRimDroite.position.x &&
            model.position.y < colRimCentre.position.y &&
            model.position.y > colRimCentre.position.y - 1 &&
            model.position.z >= colPanneau.position.z && peutCompter) {

            pointage++;
            peutCompter = false;

            console.log('Pointage : ' + pointage);

            // Intégrer Son ou animation ou VFX pour avoir compté un point

        }
    }

    detecterCollisionObjets() {
        console.log('collision objet wow bravo')
    }

    ajusterPointage() {

    }

    animate() {
        if (model !== undefined) {

            if (ballonPris !== true) {

                model.__dirtyPosition = true;
                model.setLinearVelocity(new THREE.Vector3(0, 0, 0));
                model.setAngularVelocity(new THREE.Vector3(0, 0, 0));

            }
        }

        if (panier !== undefined) {

            panier.scale.set(3, 3, 3);
            panier.position.z = -35;
            panier.position.y = -10;
            panier.rotation.y = -Math.PI / 2;
        }

        scene.simulate();
        renderer.render(scene, camera);
        requestAnimationFrame(this.animate.bind(this));
        render_stats.update();
    }

}
