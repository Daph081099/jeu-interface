export class Logo extends createjs.Bitmap{

    constructor(atlas){
        super(atlas);

        this.alpha = 0;
    }

    apparaitre(){

        createjs.Tween
            .get(this)
            .wait(1500)
            .to({alpha:1}, 1000, createjs.Ease.linear);
    }

    enlever(){
        createjs.Tween
            .get(this)
            .to({alpha:0}, 1000, createjs.Ease.linear)
    }
}