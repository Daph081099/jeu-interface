export class Instruction extends createjs.Bitmap{
    constructor(atlas){
        super(atlas);

        this.alpha = 0;
    }

    apparaitre(){

        createjs.Tween
            .get(this)
            .to({alpha:1}, 1000, createjs.Ease.linear);
    }

    enlever(){
        createjs.Tween
            .get(this)
            .to({alpha:0}, 1000, createjs.Ease.linear)
    }
}