export class Titre extends createjs.Bitmap{

    constructor(atlas){
        super(atlas);

        this.alpha = 0;
    }

    apparaitre(){

        createjs.Tween
            .get(this)
            .wait(200)
            .to({alpha:1}, 1000, createjs.Ease.linear);
    }

    bouger(){

        createjs.Tween
            .get(this)
            .wait(500)
            .to({y:150}, 1000, createjs.Ease.backOut)
    }

    enlever(){
        createjs.Tween
            .get(this)
            .to({alpha:0}, 1000, createjs.Ease.linear)
    }

}