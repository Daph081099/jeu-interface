import {Titre} from "./Titre.js";
import {Logo} from "./Logo.js";
import {Instruction} from "./Instruction.js";
import {Arcade} from "./Arcade.js";
import {Ocean} from "./Ocean.js";
import {Volcan} from "./Volcan.js";
import {Jungle} from "./Jungle.js";
import {Espace} from "./Espace.js";

let canvas, instruction1, instruction2, instruction3;

export class Debut {
    constructor() {

        this.socket = io("https://dstonge.dectim.ca:3002", {
            query: { type: 'jeu' } // identification en tant que jeu
        });

        console.log('Debut');

        this.stage = null;

        canvas = document.querySelector('#canvas-jeu');
        instruction1 = document.querySelector('.instruction1');
        instruction2 = document.querySelector('.instruction2');
        instruction3 = document.querySelector('.instruction3');

        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;

        canvas.style.backgroundColor = '#01002C';



        this.parametres = {
            manifeste: "ressources/manifest.json"
        };

        this.charger();

        this.socket.on("afficherInstruction", () => {
            console.log('Afficher Instructions Dans le jeu wow ca marche wooooooooo');

            this.afficherInstruction();
        });

        this.socket.on("partirJeu", () => {
            console.log('le jeu affiche');

            this.partirJeu();
        });

        this.socket.on("partirArcade", () => {
            console.log('arcade');

            this.partirArcade();
        });

        this.socket.on("partirOcean", () => {
            console.log('ocean');

            this.partirOcean();
        });

        this.socket.on("partirVolcan", () => {
            console.log('volcan');

            this.partirVolcan();
        });

        this.socket.on("partirJungle", () => {
            console.log('jungle');

            this.partirVolcan();
        });

        this.socket.on("partirEspace", () => {
            console.log('espace');

            this.partirEspace();
        });


    }

    charger() {

        this.chargeur = new createjs.LoadQueue();
        this.chargeur.installPlugin(createjs.Sound);
        this.chargeur.addEventListener("complete", this.creerStage.bind(this));
        this.chargeur.addEventListener('error', this.abandonner.bind(this));
        this.chargeur.loadManifest(this.parametres.manifeste);

    }

    abandonner(e) {
        alert("L'élément suivant n'a pu être chargé: " + e.src);
    }

    creerStage() {

        this.stage = new createjs.StageGL(canvas, {'antialias': true});
        this.stage.setClearColor('#01002C');

        createjs.Ticker.addEventListener('tick', e => this.stage.update(e));
        createjs.Ticker.timingMode = createjs.Ticker.RAF_SYNCHED;
        createjs.Ticker.framerate = 60;

        this.animationAccueil();


    }

    animationAccueil(){
        this.titre = new Titre(this.chargeur.getResult("nom"));
        this.stage.addChild(this.titre);
        this.titre.x = canvas.width / 2 - this.titre.getBounds().width / 2;
        this.titre.y = 500;
        this.titre.apparaitre();
        this.titre.bouger();

        this.logo = new Logo(this.chargeur.getResult('logo'));
        this.stage.addChild(this.logo);
        this.logo.x = 775;
        this.logo.y = 400;
        this.logo.apparaitre();

        // Sons
        this.musiqueAmbiance = createjs.Sound.play('musique-main', {loop:-1});
        this.musiqueAmbiance.volume = 0.2;



    }

    afficherInstruction(){
        console.log('jachiffe la function');

        this.titre.enlever();
        this.logo.enlever();

        setTimeout( this.afficherInstruction1.bind(this), 2000);
    }

    afficherInstruction1(){

        this.instruction = new Instruction(this.chargeur.getResult('instruction1'));
        this.stage.addChild(this.instruction);
        this.instruction.x = 400;
        this.instruction.y = 50;
        this.instruction.apparaitre();


        this.socket.on("afficherInstruction2", () => {

            this.afficherInstruction2();
        });
    }

    afficherInstruction2(){

        this.instruction.enlever();

        this.instruction2 = new Instruction(this.chargeur.getResult('instruction2'));
        this.stage.addChild(this.instruction2);
        this.instruction2.x = 400;
        this.instruction2.y = 50;
        this.instruction2.apparaitre();

        this.socket.on("afficherInstruction3", () => {

            this.afficherInstruction3();
        });
    }

    afficherInstruction3(){

        this.instruction2.enlever();

        this.instruction3 = new Instruction(this.chargeur.getResult('instruction3'));
        this.stage.addChild(this.instruction3);
        this.instruction3.x = 400;
        this.instruction3.y = 50;
        this.instruction3.apparaitre();

        this.socket.on("partirJeu", () => {
            console.log('le jeu affiche');

            this.partirJeu();
        });
    }

    partirJeu(){
        this.stage.removeAllChildren();

        // Tableau
        this.choix = new Instruction(this.chargeur.getResult('choix'));
        this.stage.addChild(this.choix);
        this.choix.x = 400;
        this.choix.y = 50;
        this.choix.apparaitre();

        setInterval(this.environnementTableau.bind(this), 500)

    }

    environnementTableau(){
        // Arcade
        this.arcade = new Instruction(this.chargeur.getResult('arcade'));
        this.stage.addChild(this.arcade);
        this.arcade.x = 475;
        this.arcade.y = 300;
        this.arcade.apparaitre();

        // Ocean
        this.ocean = new Instruction(this.chargeur.getResult('ocean'));
        this.stage.addChild(this.ocean);
        this.ocean.x = 825;
        this.ocean.y = 300;
        this.ocean.apparaitre();


        // Volcan
        this.volcan = new Instruction(this.chargeur.getResult('volcan'));
        this.stage.addChild(this.volcan);
        this.volcan.x = 1175;
        this.volcan.y = 300;
        this.volcan.apparaitre();


        //Jungle
        this.jungle = new Instruction(this.chargeur.getResult('jungle'));
        this.stage.addChild(this.jungle);
        this.jungle.x = 625;
        this.jungle.y = 600;
        this.jungle.apparaitre();

        //Espace
        this.espace = new Instruction(this.chargeur.getResult('espace'));
        this.stage.addChild(this.espace);
        this.espace.x = 1025;
        this.espace.y = 600;
        this.espace.apparaitre();
    }

    partirArcade(){
        console.log('allloooo arcadeeee');

        canvas.style.display = 'none';

        new Arcade();

        this.musiqueAmbiance.stop();
    }


    partirOcean(){
        console.log('allloooo oceannnnn');

        canvas.style.display = 'none';

        new Ocean();

        this.musiqueAmbiance.stop();
    }

    partirVolcan(){
        console.log('allloooo volcannn');

        canvas.style.display = 'none';

        new Volcan();

        this.musiqueAmbiance.stop();
    }

    partirJungle(){
        console.log('allloooo junglee');

        canvas.style.display = 'none';

        new Jungle();

        this.musiqueAmbiance.stop();
    }

    partirEspace(){
        console.log('allloooo espace');

        canvas.style.display = 'none';

        new Espace();

        this.musiqueAmbiance.stop();
    }



}